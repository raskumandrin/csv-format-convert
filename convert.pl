#!/usr/bin/perl
use strict;

my $read_title_mode = 0;
my $title = '';
my $subtitle = '';

my $i = 0;

while (<>) {
	chomp;
	if (/================/) {
		$read_title_mode = 1;
		next;
	}
	
	if ($read_title_mode) {
		$title = $_;
		$read_title_mode = 0;
		next;
	}

	if (/----------------/ or /^\s*$/) {
		next;
	}
	
	if ( $i == 1 ) {
		$i = 0;
		print qq("$title", "$subtitle", "$_"\n);
	}
	else {
		$i++;
		$subtitle = $_;
	}

}