https://www.odesk.com/jobs/Script-create-CSV-File_%7E01e8ee087c5a293cc2?feed=1

Script for creating CSV

I have an input file, and I want an output CSV file out of it. Basic string operations. The script/program should be in Python/Perl/Java.

Example:

INPUT:
```

===============================================================
Chapter - A
---------------------------------------------------------------
Lesson 1
Morpheus
Lesson 2
Neo
Appendix
Trinity
Bibliography
Cypher

===============================================================
Manage and Collaborate
---------------------------------------------------------------
Length
1000 mm
Width
200 mm
Height
300 mm
Jetpack
No
Nitrous
Yes
```

OUTPUT:
```
"Chapter - A", "Lesson 1", "Morpheus"
"Chapter - A", "Lesson 2", Neo"
"Chapter - A", "Appendix", Trinity"
"Chapter - A", "Bibliography", Cypher"
"Manage and Collaborate", "Length", "1000 mm"
"Manage and Collaborate", "Width", "200 mm"
"Manage and Collaborate", "Height", "300 mm"
"Manage and Collaborate", "Jetpack", "No"
"Manage and Collaborate", "Nitrous", "Yes"
```